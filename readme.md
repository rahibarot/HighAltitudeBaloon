
Download git bash: https://git-scm.com/downloads

learn Git: https://try.github.io/


After you download :
                       1 go to Desktop: HOLD shift+Right-click

                       2 Git Bash Here

                       3 mkdir name_of_folder

                       4 cd name_of_folder

                       5 git init

                       6 git clone

                      https://gitlab.com/rahibarot/HighAltitudeBaloon.git

                       7 ls

                       8 cd HighAltitudeBaloon

                       9 ls (and you can see all the folders to go in each folder 'cd name_of_the_folder')

To pull(AKA download) the updates in the git lab AKA repository:

                    1 git status

                    2 git pull

To push(AKA upload) to Repository:

                    1 git status

                    2 git add .

                    3 git commit -m "message here"

                    4 git push -u origin master
