#include<StemHab.h>
#include<SD.h>

File myFile;
String fileName;

unsigned long _time = 0;

const String headers = "(runHour:runMin:runSec),(gpsHour:gpsMin:gpsSecond),(gpsMonth/gpsDay/gpsYear),fix,fixQuality,latitude, longitude,latitudeDegrees,longitudeDegrees,altitude,speed(knots),angle,satellites,rawPressure,pressure,rawIntTemp,intTemp(C),intTemp(F),rawExtTemp,extTemp(C),extTemp(F)";

const float sleepDuration = 2;

void setup() {
  Serial.begin(9600);

  startGPS();

  if (!SD.begin(4) ) {
    Serial.println("Card failed, or not present");
    while(true);
  }

  String _name = ".csv";
  for(int i = 0;;i++) {
    _name = "HAB"+String(i)+".csv";
    if (!SD.exists(_name) )
      break;
  }

  Serial.println("Using filename " + _name);
  
  File hab = SD.open(_name,FILE_WRITE);
  for(int j = 0; j < headers.length(); j++)
    hab.write(headers.charAt(j) );
  hab.close();

  fileName = _name;

  speakerStartSound();
}

void loop() {
  delay(sleepDuration * 1000);
  _time += sleepDuration;
  
  int runHour = (int) (_time / 3600);
  int runMin = (int) ( (_time / 60) % 60);
  int runSec = _time % 60;

  String sdData = String(runHour) + ":" 
                         + (runMin<=9?"0"+String(runMin):String(runMin) ) + ":"
                         + (runSec<=9?"0"+String(runSec):String(runSec) ) ;

  int rawPressure = getRawPressure();

  sdData += "," + String(getGpsData() );
  sdData += "," + String(rawPressure);
  sdData += "," + String(getPressure(rawPressure) );

  int rawTemp = getRawTemp(0);
  int tempC = getTemp(rawTemp,3.3);
  int tempF = getTempF(tempC);
  sdData += "," + String(rawTemp);
  sdData += "," + String(tempC);
  sdData += "," + String(tempF);
  rawTemp = getRawTemp(1);
  tempC = getTemp(rawTemp,3.3);
  tempF = getTempF(tempC);
  sdData += "," + String(rawTemp);
  sdData += "," + String(tempC);
  sdData += "," + String(tempF);

  File hab = SD.open(fileName,FILE_WRITE);
  hab.write('\n');
  for(int i = 0; i < sdData.length(); i++)
    hab.write(sdData.charAt(i) );
  hab.close();

  Serial.println(sdData);
}
