/* STEM HAB Project 2017
 *  [Arduino Sender Code]
 *    Code will send over data recieved from the sensors to the SD Logger Arduino
 *    StemHab Library contains the code used to retrieve the data from the sensors
 *  By: Sam Athanas
 *  Email: redspace200@gmail.com
 */
#include <avr/pgmspace.h>
#include"StemHab.h"

// File name to use for the data logging.
//  A new file will be created each time the arduino is reset, the old files will still remain
//  A number indicating the file index number will be present at the end of each file name.
//  EX: HAB_2 <-- Index Number
//  The latest index number is the latest data log
//  Note that the timestamp of each file will be incorrect,
//    so you will have to open each file and read the gps time or the rtc time to determine when the data was logged

/* The circuit:
 * analog sensors on analog ins 0, 1, and 2
 * SD card attached to SPI bus as follows:
 ** MOSI - pin 11
 ** MISO - pin 12
 ** CLK - pin 13
 ** CS - pin 4 (for MKRZero SD: SDCARD_SS_PIN)
*/


// Pins to talk with the SD loggger arduino
//****SoftwareSerial SdSerial(0,1); // RX,TX

unsigned long _time = 0;


void setup() {
  Serial.begin(9600);
//****  SdSerial.begin(9600);

  startGPS();
//  startPressure();
   speakerStartSound();

//***  startRTC();

  pinMode(LED_BUILTIN, OUTPUT);
  digitalWrite(LED_BUILTIN, LOW);


  digitalWrite(LED_BUILTIN, HIGH);

}

void loop() {

  int runHour = (int) (_time / 3600);
  int runMin = (int) ( (_time / 60) % 60);
  int runSec = _time % 60;

  String sd_write = String(runHour) + ":" 
                         + (runMin<=9?"0"+String(runMin):String(runMin) ) + ":"
                         + (runSec<=9?"0"+String(runSec):String(runSec) ) ;
  // GPS
  // hour,minute,second,month,day,year,fix,fixQuality,latitude,longitude,latitudeDegrees,longitudeDegrees,altitude,speed(knots),angle,satellites

  Serial.println(F("1"));
  sd_write += "," + getGpsData();

  Serial.println(F("a"));
  sd_write += "," + String( getTemp(0, 3.3) );

  Serial.println(F("2"));
  sd_write += "," + String( getTemp(1, 3.3));
  

  //Serial.println(sd_write);
  Serial.println(sd_write + "$\n");
  // Delay time before each update in seconds
  // formal UPDATE_DELAY 2
  delay( 2 * 1000);
  _time += 2 ;

}
