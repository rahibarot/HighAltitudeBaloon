#ifndef _STEM_HAB_
#define _STEM_HAB_

bool SET_DEBUG(bool);

//void initSerial();
//void initI2C();

//void startLED();
//void setLED(bool); // <----
//void flashLED();

// void setSpeaker(int); // <----
void speakerStartSound();

/*
bool startSD();
bool sdActive();
bool fileExists(String);
bool readFile(String);
bool writeFile(String,String); // <----
*/

float getRawTemp( int );
float getTemp( float, float );
float getTempF( float );

int getRawPressure();
float getPressure( int );

/*
bool startRTC();
bool RTCRunning();
void setRTCTime();

String getDate(); // <---
String getDate(char);
String getTime(); // <---
String getTime(char);
*/

void startGPS();
String getGpsData(); // <----

// String getMicData(); // <---

#endif // _STEM_HAB_
